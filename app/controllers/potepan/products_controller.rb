class Potepan::ProductsController < ApplicationController
  DISPLAY_MAXIMUM_RELATED_PRODUCTS_NUMBER = 4

  def show
    @product = Spree::Product.friendly.find(params[:id])
    @related_products = @product.related_products.sample(DISPLAY_MAXIMUM_RELATED_PRODUCTS_NUMBER)
  end
end
