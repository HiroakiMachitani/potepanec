require 'rails_helper'

Rails.feature "Product", type: :system do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, taxonomy: taxonomy, parent_id: taxonomy.root.id) }
  let(:other_taxon) { create(:taxon, taxonomy: taxonomy, parent_id: taxonomy.root.id) }
  let(:product) { create(:product, taxons: [taxon], price: 123) }
  let(:other_product) { create(:product, taxons: [other_taxon], price: 789) }
  let!(:related_product) { create(:product, taxons: [taxon], price: 456) }

  context "商品詳細ページにアクセスしたとき" do
    before do
      visit potepan_product_path(product.id)
    end

    it "商品詳細と関連商品が表示され,関連しない商品は表示されない" do
      within('.media-body') do
        expect(page).to have_content product.name
        expect(page).to have_content product.display_price
        expect(page).to have_content product.description
      end
      within('.productBox') do
        expect(page).not_to have_content product.name
        expect(page).not_to have_content product.display_price
        expect(page).to have_content related_product.name
        expect(page).to have_content related_product.display_price
        expect(page).not_to have_content other_product.name
        expect(page).not_to have_content other_product.display_price
      end
    end
  end

  context "一覧へ戻るをクリックしたとき" do
    before do
      visit potepan_product_path(product.id)
      click_on "一覧ページへ戻る"
    end

    it "商品が属するカテゴリー一覧ページが表示される" do
      expect(current_path).to eq potepan_category_path(taxon.id)
    end
  end

  context "関連商品をクリックしたとき" do
    before do
      visit potepan_product_path(product.id)
      within(".productBox") do
        click_on related_product.name
      end
    end

    it "その商品の詳細ページが表示される" do
      within('.media-body') do
        expect(page).to have_content related_product.name
        expect(page).to have_content related_product.display_price
        expect(page).to have_content related_product.description
      end
    end
  end

  context "関連商品が5つあるとき" do
    before do
      create_list(:product, 5, taxons: [taxon])
      visit potepan_product_path(product.id)
    end

    it "4つのみ表示される" do
      expect(all('.productBox').size).to eq 4
    end
  end
end
