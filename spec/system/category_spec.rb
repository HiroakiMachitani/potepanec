require 'rails_helper'

RSpec.feature "Category", type: :system do
  let(:taxonomy) { create(:taxonomy, name: "Category") }
  let(:taxon) { create(:taxon, name: "Bag", taxonomy: taxonomy) }
  let!(:other_taxon) { create(:taxon, name: "Shirts", taxonomy: taxonomy) }
  let!(:product) do
    create(:product, name: "Ruby on Rails Bag", price: "12.34", taxons: [taxon])
  end
  let!(:other_product) do
    create(:product, name: "Ruby on Rails Shirt", price: "56.78", taxons: [other_taxon])
  end

  context "Bagカテゴリーページが表示されているとき" do
    before do
      visit potepan_category_path(taxon.id)
    end

    it '商品カテゴリーとBagカテゴリーに属した商品が表示される' do
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
      expect(page).not_to have_content other_product.name
      expect(page).not_to have_content other_product.display_price
      within('.side-nav') do
        click_on taxonomy.name
        expect(page).to have_content taxonomy.name
        expect(page).to have_content taxon.name
        expect(page).to have_content taxon.product_ids.count
        expect(page).to have_content other_taxon.name
        expect(page).to have_content other_taxon.product_ids.count
        expect(page).to have_link taxon.name, href: potepan_category_path(taxon.id)
        expect(page).to have_link other_taxon.name, href: potepan_category_path(other_taxon.id)
      end
    end
  end

  context 'Shirtsカテゴリーをクリックしたとき' do
    before do
      visit potepan_category_path(taxon.id)
      click_on taxonomy.name
      click_on other_taxon.name
    end

    it 'Shirtsカテゴリーページへ飛ぶ' do
      expect(current_path).to eq potepan_category_path(other_taxon.id)
    end
  end

  context '商品をクリックしたとき' do
    before do
      visit potepan_category_path(taxon.id)
      find(".productBox").click
    end

    it '商品詳細ページが表示される' do
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
    end
  end
end
