require 'rails_helper'

RSpec.describe "Potepan::Products", type: :request do
  let(:category) { create(:taxonomy, name: "Category") }
  let(:taxon) { create(:taxon, name: "Taxon", taxonomy: category) }
  let(:product) { create(:product, taxons: [taxon], name: "Product", price: "12.34") }

  before do
    get potepan_product_path(product.id)
  end

  it "商品の各項目が表示される" do
    expect(response.body).to include product.name
    expect(response.body).to include product.display_price.to_s
    expect(response.body).to include product.description
  end

  it "リクエストが成功する" do
    expect(response.status).to eq 200
  end
end
