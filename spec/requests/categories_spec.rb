require 'rails_helper'

RSpec.describe "Categories_request", type: :request do
  describe "GET categories#show" do
    let(:category)   { create(:taxonomy, name: "Category") }
    let!(:bag)       { create(:taxon, name: "Bag", taxonomy: category, parent: category.root) }
    let!(:rails_bag) { create(:product, taxons: [bag], name: "Ruby on Rails Bag") }

    before do
      get potepan_category_path(bag.id)
    end

    it "リクエストが成功" do
      expect(response.status).to eq 200
    end

    it "各項目が表示される" do
      expect(response.body).to include category.name
      expect(response.body).to include bag.name
      expect(response.body).to include rails_bag.name
    end
  end
end
