require 'rails_helper'

RSpec.describe Potepan::ProductDecorator, type: :model do
  let(:taxon) { create(:taxon) }
  let(:product) { create(:product, taxons: [taxon]) }
  let(:related_product) { create(:product, taxons: [taxon]) }

  describe "#related_products" do
    it "関連商品を取得する" do
      expect(product.related_products).to include related_product
    end

    it "関連商品に表示している商品を含めない" do
      expect(product.related_products).not_to include product
    end
  end
end
