require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "#full_title" do
    let(:base_title) { "BIGBAG Store" }

    context "page_titleがないとき" do
      it "base_titleが表示される" do
        expect(full_title).to eq base_title
      end
    end

    context "page_titleがあるとき" do
      it "full_titleが表示される" do
        expect(full_title("test")).to eq "test - #{base_title}"
      end
    end
  end
end
